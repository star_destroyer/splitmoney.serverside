﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using SplitMoney.ServerModel;
using SplitMoney.ServerModel.Components.Conversations;
using SplitMoney.ServerModel.Components.Repositories;
using SplitMoney.ServerModel.Conversations;

namespace SplitMoney.Business.Conversations
{
    public class ConversationManager : IConversationsManager
    {
	    private readonly IConversationsRepository conversationsRepository;
	    private readonly IUsersRepository usersRepository;
	    private readonly ILogger logger;
		
		public ConversationManager(IConversationsRepository conversationsRepository, IUsersRepository usersRepository, ILoggerFactory loggerFactory)
	    {
		    this.conversationsRepository = conversationsRepository;
		    this.usersRepository = usersRepository;
		    this.logger = loggerFactory.CreateLogger<ConversationManager>();
	    }

	    public async Task<OperationResult<Conversation>> CreateOrUpdateConversationAsync(long owner, IEnumerable<long> participants, string name, Guid? id)
	    {
		    var participantIds = GetFullParticipantList(owner, participants);
		    if (participantIds.Length < 2)
		    {
			    return new OperationResult<Conversation>
			    {
				    Message = $"В беседе должно быть хотя бы два уникальных пользователя",
				    Status = ResultStatus.ArgumentError
			    };
		    }
		    var missingUsers = await GetMissingParticipantIdsAsync(participantIds);
		    if (missingUsers.Any())
		    {
			    return new OperationResult<Conversation>
				{
				    Message = $"Some users are not registered in the system: {string.Join(", ", missingUsers)}",
				    Status = ResultStatus.NotFound
			    };
		    }
		    var conversation = await GetConversation(participantIds, id);
		    if (!await TryCreateConversationAsync(conversation))
		    {
			    return new OperationResult<Conversation> {Content = conversation, Status = ResultStatus.ServerError, Message = "Не удалось создать беседу"};
		    }
		    await TryUpdateConversationName(owner, conversation.Id, name);

		    return new OperationResult<Conversation> { Content = conversation, Status = ResultStatus.Ok};
	    }

	    private long[] GetFullParticipantList(long owner, IEnumerable<long> participants)
	    {
		    return participants.Union(new[] {owner}).Distinct().ToArray();
	    }

	    private async Task<List<long>> GetMissingParticipantIdsAsync(IEnumerable<long> participants)
	    {
		    var missingParticipants = new List<long>();
		    foreach (var id in participants)
		    {
			    if (await usersRepository.GetUserAsync(id) == null)
			    {
				    missingParticipants.Add(id);
			    }
		    }
		    return missingParticipants;
	    }

	    private async Task<Conversation> GetConversation(long[] participantIds, Guid? conversationId)
	    {
		    var conversationType = participantIds.Length > 1 ? ConversationType.Group : ConversationType.Dialog;
		    if (conversationType == ConversationType.Dialog)
		    {
			    var dialog = await GetExistingDialog(participantIds);
			    if (dialog != null)
			    {
				    return dialog;
			    }
		    }
		    var conversation = new Conversation
			{ 
				Id = conversationId ?? Guid.NewGuid(),
			    Participants = participantIds,
			    Type = conversationType
		    };
		    return conversation;
	    }

	    private async Task<Conversation> GetExistingDialog(long[] participants)
	    {
		    var userConversations = await conversationsRepository.GetConversationsAsync(participants.First());
		    return userConversations.FirstOrDefault(conversation => !conversation.Participants.Except(participants).Any());
	    }

	    private async Task<bool> TryCreateConversationAsync(Conversation conversation)
	    {
		    try
		    {
			    await conversationsRepository.CreateOrUpdateConversationAsync(conversation);
		    }
		    catch (Exception e)
		    {
			    logger.LogError(new EventId(e.HResult), e, "Could not create conversation in mongoRepository");
			    return false;
		    }
		    return true;
	    }

	    private async Task TryUpdateConversationName(long owner, Guid conversationId, string name)
	    {
		    try
		    {
			    await usersRepository.AddOrUpdateConversationNameAsync(owner, conversationId, name);
		    }
			catch(Exception e)
		    {
			    logger.LogError(new EventId(e.HResult), e, $"Could not create conversation name preference in mongoRepository ---> Conversation.Id: {conversationId}");
		    }
	    }
    }
}
