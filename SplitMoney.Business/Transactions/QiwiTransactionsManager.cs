﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using SplitMoney.QiwiClient;
using SplitMoney.ServerModel.Components.Repositories;
using SplitMoney.ServerModel.Components.Transactions;
using SplitMoney.ServerModel.Transactions;
using SplitMoney.ServerModel.Users;

namespace SplitMoney.Business.Transactions
{
    public class QiwiTransactionsManager : ITransactionManager
    {
		private readonly ITransactionRepository transactionRepository;
	    private readonly IConversationsRepository conversationsRepository;
	    private readonly IUsersRepository usersRepository;
	    private readonly IQiwiClientFactory qiwiClientFactory;

	    public QiwiTransactionsManager(
			ITransactionRepository transactionRepository, 
			IConversationsRepository conversationsRepository, 
			IUsersRepository usersRepository, 
			IQiwiClientFactory qiwiClientFactory)
	    {
		    this.transactionRepository = transactionRepository;
		    this.conversationsRepository = conversationsRepository;
		    this.usersRepository = usersRepository;
		    this.qiwiClientFactory = qiwiClientFactory;
	    }

	    public async Task<Transaction> CreateTransactionAsync(
			long userId, 
			Guid recipientConversation, 
			Guid paymentMethodId,
			string comment, 
			decimal amount,
		    Currency currency)
	    {
		    var paymentMethod = (await usersRepository.GetPaymentMethodsAsync(userId))
			    .First(method => method.Type == PaymentType.QiwiTransfer); //TODO: брать по paymentMethodId, а не первый попавшийся
			var qiwiWallet = new QiwiWallet(paymentMethod);

		    var code = await qiwiClientFactory
				.BeginAuthorizationAsync(qiwiWallet.PhoneNumber.ToString(), CancellationToken.None);
			var transaction = await StoreTransaction(recipientConversation, amount, comment, currency, qiwiWallet, code);

		    return transaction;
	    }

	    public async Task<(Transaction transaction, string message)> ConfirmTransactionAsync(Guid transactionId, Confirmation confirmationType, Dictionary<string, string> confirmationOptions)
	    {
		    if (confirmationType != Confirmation.ConfirmationCode)
		    {
			    throw new InvalidOperationException($"Для Qiwi-транзакций доступен только способ подтверждения {Confirmation.ConfirmationCode}");
		    }
		    var transaction = await transactionRepository.GetTransactionAsync(transactionId);
		    if (transaction == null)
		    {
			    throw new ArgumentException("Не существует транзакции с таким Id");
		    }
		    if (transaction.Properties.ContainsKey(TransactionProperties.QiwiTransactions))
		    {
			    var transactions = (IEnumerable<QiwiTransaction>) transaction.Properties[TransactionProperties.QiwiTransactions];
			    var msg = transaction.Status == TransactionStatus.Confirmed ? "Ok" : transactions.GetMessage();
			    return (transaction, msg);
		    }
			var qiwiClient = await AuthoriseAndGetQiwiClientAsync(transaction, confirmationOptions);
		    var recipients = await GetRecipientsAsync(transaction);
			var qiwiTransactions = new List<QiwiTransaction>();
			foreach(var recipient in recipients)
			{
				var qiwiTransaction = await qiwiClient.SendMoneyAsync(recipient.id.ToString(), recipient.amount, CancellationToken.None);
				qiwiTransactions.Add(qiwiTransaction);
			}
		    transaction.Properties[TransactionProperties.QiwiTransactions] = qiwiTransactions;
		    var isSuccess = qiwiTransactions.All(qt => qt.IsSuccess);
		    transaction.Status = isSuccess ? TransactionStatus.Confirmed : TransactionStatus.Rejected;
		    transaction.Finished = DateTime.UtcNow;
		    await transactionRepository.CreateOrReplaceTransactionAsync(transaction);
		    var message = isSuccess ? "Ok" : qiwiTransactions.GetMessage();

		    return (transaction, message);
	    }

	    private async Task<IEnumerable<(long id, decimal amount)>> GetRecipientsAsync(Transaction transaction)
	    {
			var conversation = await conversationsRepository.GetConversationAsync(transaction.ConversationId);
		    var recipients = conversation.Participants.Where(p => p != transaction.From).ToList();
		    var amountPerPerson = transaction.Amount / recipients.Count;
		    return recipients.Select(r => (r, amountPerPerson));
	    }

	    private async Task<IQiwiClient> AuthoriseAndGetQiwiClientAsync(Transaction transaction, Dictionary<string, string> confirmationOptions)
	    {
		    var code = (string) transaction.Properties[TransactionProperties.QiwiAuthCode];
		    var smsCode = confirmationOptions["sms-code"];
		    var qiwiClient =
			    await qiwiClientFactory.EndAuthorizationAsync(code, smsCode, transaction.From.ToString(), CancellationToken.None);
		    return qiwiClient;
	    }

	    private async Task<Transaction> StoreTransaction(
			Guid recipientConversation, 
			decimal amount, 
			string comment,
			Currency currency, 
			QiwiWallet qiwiWallet,
		    string code)
	    {
		    var transaction = new Transaction
		    {
			    From = qiwiWallet.PhoneNumber,
				Comment = comment,
			    Amount = amount,
				Created = DateTime.UtcNow,
			    Status = TransactionStatus.Pending,
			    ConversationId = recipientConversation,
			    Currency = currency,
			    Properties = new Dictionary<string, object>
			    {
				    {TransactionProperties.QiwiAuthCode, code}
			    }
		    };
		    await transactionRepository.CreateOrReplaceTransactionAsync(transaction);
		    return transaction;
	    }
    }
}
