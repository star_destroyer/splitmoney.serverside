using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SplitMoney.ServerModel.Components.Repositories;
using SplitMoney.ServerModel.Components.Transactions;
using SplitMoney.ServerModel.Transactions;

namespace SplitMoney.Business.Transactions
{
	public class CashTransactionsManager : ITransactionManager
	{
		private readonly ITransactionRepository transactionRepository;

		public CashTransactionsManager(ITransactionRepository transactionRepository)
		{
			this.transactionRepository = transactionRepository;
		}


		public async Task<Transaction> CreateTransactionAsync(long userId, Guid recipientConversation, Guid paymentMethodId, string comment, decimal amount, Currency currency)
		{
			var transaction = await StoreTransaction(recipientConversation, comment, amount, currency, userId);

			return transaction; ;
		}

		private async Task<Transaction> StoreTransaction(
			Guid recipientConversation,
			string comment,
			decimal amount,
			Currency currency,
			long userId)
		{
			var transaction = new Transaction
			{
				Comment = comment,
				From = userId,
				Amount = amount,
				Created = DateTime.UtcNow,
				Status = TransactionStatus.Pending,
				ConversationId = recipientConversation,
				Currency = currency,
				Type = PaymentType.Cash
			};
			await transactionRepository.CreateOrReplaceTransactionAsync(transaction);
			return transaction;
		}

		public async Task<(Transaction transaction, string message)> ConfirmTransactionAsync(Guid transactionId, Confirmation confirmationType, Dictionary<string, string> confirmationOptions)
		{
			if(confirmationType != Confirmation.Reject && confirmationType != Confirmation.Confirm)
				throw new ArgumentException($"������������� �������� ���������� �������� {confirmationType} �� ��������������");
			var transaction = await transactionRepository.GetTransactionAsync(transactionId);

			transaction.Status =
				confirmationType == Confirmation.Reject
					? TransactionStatus.Rejected
					: confirmationType == Confirmation.Confirm
						? TransactionStatus.Confirmed
						: throw new ArgumentException();
			transaction.Finished = DateTime.UtcNow;
			await transactionRepository.CreateOrReplaceTransactionAsync(transaction);
			return (transaction, "Ok");
		}
	}
}