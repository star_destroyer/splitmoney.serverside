﻿using System;
using SplitMoney.QiwiClient;
using SplitMoney.ServerModel.Components.Repositories;
using SplitMoney.ServerModel.Components.Transactions;
using SplitMoney.ServerModel.Transactions;

namespace SplitMoney.Business.Transactions
{
    public class TransactionManagerFactory : ITransactionManagerFactory
    {
	    private readonly ITransactionRepository transactionRepository;
	    private readonly IConversationsRepository conversationsRepository;
	    private readonly IUsersRepository usersRepository;
	    private readonly IQiwiClientFactory qiwiClientFactory;

	    public TransactionManagerFactory(
			ITransactionRepository transactionRepository, 
			IConversationsRepository conversationsRepository, 
			IUsersRepository usersRepository, 
			IQiwiClientFactory qiwiClientFactory)
	    {
		    this.transactionRepository = transactionRepository;
		    this.conversationsRepository = conversationsRepository;
		    this.usersRepository = usersRepository;
		    this.qiwiClientFactory = qiwiClientFactory;
	    }

	    public ITransactionManager GetTransactionManager(PaymentType paymentType)
	    {
		    switch (paymentType)
		    {
			    case PaymentType.QiwiTransfer:
				    return new QiwiTransactionsManager(
						transactionRepository,
						conversationsRepository,
						usersRepository,
						qiwiClientFactory);
			    case PaymentType.Cash:
				    return new CashTransactionsManager(
					    transactionRepository
				    );
			    default:
				    throw new ArgumentOutOfRangeException(nameof(paymentType), paymentType, null);
		    }
	    }
    }
}
