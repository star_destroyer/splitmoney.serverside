﻿using System;
using System.Text;

namespace SplitMoney.Business.Utilities
{
    internal static class Base64Encoder
    {
        public static string Encode(string plainText) => Convert.ToBase64String(Encoding.UTF8.GetBytes(plainText));

        public static string Decode(string base64EncodedData) => Encoding.UTF8.GetString(Convert.FromBase64String(base64EncodedData));
    }
}