﻿using System;
using System.Linq;
using SplitMoney.ServerModel.Components.Utilities;

namespace SplitMoney.Business.Utilities
{
    public class GuidBasedTokenGenerator : ITokenGenerator
    {
	    private const int TokenLengthBytes = 20;
		private static readonly Random random = new Random();

        public string NewToken()
        {
	        var tokenBytes = new byte[TokenLengthBytes];
	        random.NextBytes(tokenBytes);

            return string.Concat(tokenBytes.Select(b => b.ToString("X2")));
        }
    }
}
