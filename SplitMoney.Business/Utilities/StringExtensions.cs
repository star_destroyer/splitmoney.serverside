﻿using System;
using System.Linq;

namespace SplitMoney.Business.Utilities
{
    public static class StringExtensions
    {
        public static long ToInt64PhoneNumber(this string phone)
        {
            if (phone.Length == 11 && long.TryParse(phone, out var number))
            {
                return number;
            }

            var cleanedPhone = new string(phone.Where(char.IsDigit).ToArray());
            if(cleanedPhone.Length != 11)
            {
                throw new ArgumentException("Input is not a correct mobile phone");
            }
            return long.Parse(cleanedPhone);
        }
    }
}
