﻿using System.Text.RegularExpressions;
using SplitMoney.ServerModel.Components.Utilities;

namespace SplitMoney.Business.Utilities
{
    public class PhoneValidator : IPhoneValidator
    {
        private static readonly Regex PhoneValidation = new Regex(@"^8/d+", RegexOptions.Compiled);
        public bool Validate(string phone, out string formatedPhone)
        {
            formatedPhone = null;
            if (string.IsNullOrWhiteSpace(phone))
            {
                return false;
            }
            var result = PhoneValidation.IsMatch(phone);
            return result;
        }
    }
}