﻿using System;
using SplitMoney.ServerModel.Components.Utilities;

namespace SplitMoney.Business.Utilities
{
    public class DateTimeService : IDateTimeService
    {
        public DateTime Now => DateTime.Now;
        public DateTime UtcNow => DateTime.UtcNow;
    }
}