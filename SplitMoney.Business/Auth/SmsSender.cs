﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using SplitMoney.ServerModel.Components.Authentication;
using SplitMoney.ServerModel.Configuration;

namespace SplitMoney.Business.Auth
{
    public class SmsSender : IAuthenticationSmsSender
    {
		private static readonly Random Random = new Random();

	    private const string apiUri = "https://gate.smsaero.ru/";
	    private const string name = "news";
	    private const int type = 4;

	    private const string method = "send";
		
		private readonly string user;
	    private readonly string apiKey;
	    private readonly bool enableMessages;

	    public SmsSender(IAppConfig config)
	    {
		    user = config.SmsService.User;
		    apiKey = config.SmsService.ApiKey;
		    enableMessages = config.SmsService.EnableMessages;
	    }

	    public async Task<string> SendAuthenticationCodeAsync(long phone)
	    {
		    if (!enableMessages)
			    return "1010";

		    var client = new HttpClient();
			var smsCode = Random.Next(0, 9999).ToString("D4");
		    var text = WebUtility.UrlEncode($"Код доступа SplitMoney: {smsCode}");
		    var sendResult = await client.GetAsync(new Uri(new Uri(apiUri), $"{method}/?user={user}&password={apiKey}&to={phone}&type={type}&text={text}&answer=json&from={name}"));
		    var stringResult = sendResult.Content.ReadAsStringAsync();

		    return smsCode;
	    }
    }
}
