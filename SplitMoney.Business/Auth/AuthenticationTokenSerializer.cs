﻿using SplitMoney.Business.Utilities;
using SplitMoney.ServerModel.Components.Authentication;

namespace SplitMoney.Business.Auth
{
    public class AuthenticationTokenSerializer : IAuthenticationTokenSerializer
    {
        public (long phone, string token) Deserialize(string authArgument)
        {
            var authString = Base64Encoder.Decode(authArgument);
            var parts = authString.Split(':');

            return (long.Parse(parts[0]), parts[1]);
        }
    }
}
