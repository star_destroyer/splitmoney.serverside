using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using SplitMoney.ServerModel.Authentication;
using SplitMoney.ServerModel.Components.Authentication;
using SplitMoney.ServerModel.Components.Repositories;
using SplitMoney.ServerModel.Components.Utilities;
using SplitMoney.ServerModel.Transactions;
using SplitMoney.ServerModel.Users;

namespace SplitMoney.Business.Auth
{
    public class AuthenticationManager : IAuthenticationManager
    {
        private const int SessionAliveDays = 15;

        private readonly ISessionRepository sessionRepository;
	    private readonly IUsersRepository usersRepository;
        private readonly IDateTimeService dateTimeService;
        private readonly IAuthenticationSmsSender authenticationSmsSender;
        private readonly ITokenGenerator tokenGenerator;

        public AuthenticationManager(
            ISessionRepository sessionRepository, 
            IUsersRepository usersRepository, 
            IDateTimeService dateTimeService, 
            IAuthenticationSmsSender authenticationSmsSender, 
			ITokenGenerator tokenGenerator)
        {
            this.sessionRepository = sessionRepository;
	        this.usersRepository = usersRepository;
	        this.dateTimeService = dateTimeService;
	        this.authenticationSmsSender = authenticationSmsSender;
	        this.tokenGenerator = tokenGenerator;
        }

        public async Task<CodeInfo> CreateCodeAsync(long phone, CancellationToken cancellation)
        {
	        var smsCode = await authenticationSmsSender.SendAuthenticationCodeAsync(phone).ConfigureAwait(false);

            var session = new UserSession
            {
                Phone = phone,
                Code = tokenGenerator.NewToken(),
                SmsCode = smsCode
            };

            await sessionRepository.WriteAsync(session, cancellation).ConfigureAwait(false);
            return new CodeInfo
            {
                Code = session.Code
            };
        }

        public async Task<AccessTokenInfo> GetTokenAsync(long phone, string code, string smsCode, CancellationToken cancellation)
        {
	        await usersRepository.CreateUserAsync(phone);
	        await AddDefaultPaymentMethods(phone); //TODO: ������, ����� ������� �������� �������� ������
            var session = await sessionRepository.GetByPhoneAndCodeAsync(phone, code, cancellation).ConfigureAwait(false);

            if (session == null || !smsCode.Equals(session.SmsCode))
            {
                return new AccessTokenInfo
                {
                    IsSuccess = false,
                    Message = session == null 
                        ? "Session not found"
                        : "Invalid sms code"
                };
            }

            session.Token = tokenGenerator.NewToken();
            session.RefreshToken = tokenGenerator.NewToken();
            session.Expires = dateTimeService.UtcNow.AddDays(SessionAliveDays).Ticks;

            await sessionRepository.WriteAsync(session, cancellation).ConfigureAwait(false);

            return new AccessTokenInfo
            {
                IsSuccess = true,
                Token = session.Token,
                RefreshToken = session.RefreshToken
            };
        }

	    private async Task AddDefaultPaymentMethods(long phone)
	    {
		    var methods = (await usersRepository.GetPaymentMethodsAsync(phone) ?? Enumerable.Empty<PaymentMethod>()).ToArray();
			if(methods.All(m => m.Type != PaymentType.QiwiTransfer))
			{
				await usersRepository.AddPaymentMethodAsync(
					phone,
					new PaymentMethod
					{
						Type = PaymentType.QiwiTransfer,
						PaymentInfo = new Dictionary<string, string>
						{
							{QiwiWallet.PhoneNumberKey, $"{phone}"}
						}
					});
			}
			if(methods.All(m => m.Type != PaymentType.Cash))
			{
				await usersRepository.AddPaymentMethodAsync(
					phone,
					new PaymentMethod
					{
						Type = PaymentType.Cash
					});
		    }
	    }

	    public async Task<TokenValidationInfo> ValidateAsync(long phone, string token, CancellationToken cancellation)
        {
            var userSession = await sessionRepository.GetByPhoneAndTokenAsync(phone, token, cancellation).ConfigureAwait(false);
            if (userSession == null)
            {
                return new TokenValidationInfo {Result = TokenValidationResult.NotFound};
            }

            if (userSession.Expires - DateTime.UtcNow.Ticks <= 0)
            {
                return new TokenValidationInfo {Result = TokenValidationResult.Expired};
            }

            return new TokenValidationInfo
            {
                Result = TokenValidationResult.Ok,
                User = new User {Id = userSession.Phone}
            };
        }
    }
}