﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using MongoDB.Driver;

namespace SplitMoney.DataAccess.Utility
{
    public static class AsyncCursorExtensions
    {
        public static T TakeOne<T>(this IAsyncCursor<T> cursor, CancellationToken cancellation = default(CancellationToken))
        {
            if (cursor.Current != null)
                return cursor.Current.FirstOrDefault();
            return cursor.MoveNext(cancellation)
                ? cursor.Current.FirstOrDefault()
                : default(T);
        }

	    public static IEnumerable<T> TakeAll<T>(this IAsyncCursor<T> cursor)
	    {
		    if(cursor.Current == null)
				cursor.MoveNext();
		    do
		    {
			    foreach (var item in cursor.Current)
			    {
				    yield return item;
			    }
		    } while (cursor.MoveNext());
	    }
    }
}
