﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using SplitMoney.DataAccess.Storage;
using SplitMoney.DataAccess.Utility;
using SplitMoney.ServerModel.Components.Repositories;
using SplitMoney.ServerModel.Configuration;
using SplitMoney.ServerModel.Conversations;

namespace SplitMoney.DataAccess.Repositories
{
    public class ConversationsRepository : RepositoryBase<Conversation>, IConversationsRepository
    {
	    public ConversationsRepository(IAppConfig config, IClientFactory clientFactory) : base(config, clientFactory)
	    {
	    }

	    public async Task<Conversation> GetConversationAsync(Guid id)
	    {
		    var result = await Collection.FindAsync<Conversation>(
			    Builders<Conversation>.Filter.Eq(conv => conv.Id, id),
			    new FindOptions<Conversation> {Limit = 1}
		    );

		    return result.TakeOne();
	    }

	    public async Task<IEnumerable<Conversation>> GetConversationsAsync(long userId)
	    {
		    var result = await Collection.FindAsync<Conversation>(
			    Builders<Conversation>.Filter.AnyEq(
				    conv => conv.Participants, userId));

		    return result.TakeAll();
	    }

	    public async Task CreateOrUpdateConversationAsync(Conversation conversation)
	    {
		    await Collection.ReplaceOneAsync(
			    Builders<Conversation>.Filter.Eq(conv => conv.Id, conversation.Id),
				conversation,
				new UpdateOptions { IsUpsert = true }
			);
	    }
    }
}
