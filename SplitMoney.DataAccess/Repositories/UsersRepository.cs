﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SplitMoney.DataAccess.Storage;
using SplitMoney.DataAccess.Utility;
using SplitMoney.ServerModel.Components.Repositories;
using SplitMoney.ServerModel.Configuration;
using SplitMoney.ServerModel.Users;
using MongoDB.Driver;

namespace SplitMoney.DataAccess.Repositories
{
	public class UsersRepository : RepositoryBase<User>, IUsersRepository
	{
		public UsersRepository(IAppConfig config, IClientFactory clientFactory) : base(config, clientFactory)
		{
		}

		public async Task CreateUserAsync(long userId)
		{
			var user = await GetUserAsync(userId);
			if(user == null)
			{
				await Collection.InsertOneAsync(new User{Id = userId});
			}
		}

		public async Task<User> GetUserAsync(long userId)
		{
			return (await Collection.FindAsync<User>(
					Builders<User>.Filter.Eq(pref => pref.Id, userId)))
				.TakeOne();
		}
		
		public async Task AddOrUpdateConversationNameAsync(long userId, Guid id, string name)
		{
			await Collection.UpdateOneAsync(
				Builders<User>.Filter.Eq(user => user.Id, userId),
				Builders<User>.Update.Set(
					$"{nameof(User.ConversationNames)}.{id}", name),
				new UpdateOptions {IsUpsert = true});
		}

		public async Task SetLastConversationCheckTime(long userId, Guid conversationId, DateTime time)
		{
			await Collection.UpdateOneAsync(
				Builders<User>.Filter.Eq(user => user.Id, userId),
				Builders<User>.Update.Set(
					$"{nameof(User.ConversationLastCheckDates)}.{conversationId}", time),
				new UpdateOptions { IsUpsert = true });
		}

		public async Task AddPaymentMethodAsync(long userId, PaymentMethod method)
		{
			await Collection.UpdateOneAsync(
				Builders<User>.Filter.Eq(pref => pref.Id, userId),
				Builders<User>.Update.Push(
					user => user.PaymentMethods,
					method));
		}

		public async Task<IEnumerable<PaymentMethod>> GetPaymentMethodsAsync(long userId)
		{
			var result = await Collection
				.Find(Builders<User>.Filter.Eq(pref => pref.Id, userId))
				.FirstOrDefaultAsync(); //TODO projection -> paymentMethods
			return result.PaymentMethods;
		}
	}
}
