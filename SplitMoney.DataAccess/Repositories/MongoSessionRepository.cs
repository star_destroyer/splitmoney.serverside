﻿using System.Threading;
using System.Threading.Tasks;
using MongoDB.Driver;
using SplitMoney.DataAccess.Storage;
using SplitMoney.DataAccess.Utility;
using SplitMoney.ServerModel.Authentication;
using SplitMoney.ServerModel.Components.Repositories;
using SplitMoney.ServerModel.Configuration;

namespace SplitMoney.DataAccess.Repositories
{
	public class MongoSessionRepository : RepositoryBase<UserSession>, ISessionRepository
	{
		public MongoSessionRepository(IAppConfig configuration, IClientFactory clientFactory)
			: base(configuration, clientFactory)
		{
		}

		public async Task WriteAsync(UserSession session, CancellationToken cancellation)
		{
			await Collection.ReplaceOneAsync(
					Builders<UserSession>.Filter.Eq(userSession => userSession.Id, session.Id),
					session,
					new UpdateOptions {IsUpsert = true},
					cancellation)
				.ConfigureAwait(false);
		}

		public async Task<UserSession> GetByPhoneAndTokenAsync(long phone, string token, CancellationToken cancellation)
		{
			var cursor = await Collection.FindAsync(
					Builders<UserSession>.Filter.And(
						Builders<UserSession>.Filter.Eq(session => session.Phone, phone),
						Builders<UserSession>.Filter.Eq(session => session.Token, token)),
					new FindOptions<UserSession> {Limit = 1},
					cancellation)
				.ConfigureAwait(false);

			return cursor.TakeOne();
		}

		public async Task<UserSession> GetByPhoneAndCodeAsync(long phone, string code, CancellationToken cancellation)
		{
			var cursor = await Collection.FindAsync(
					Builders<UserSession>.Filter.And(
						Builders<UserSession>.Filter.Eq(session => session.Phone, phone),
						Builders<UserSession>.Filter.Eq(session => session.Code, code)),
					new FindOptions<UserSession> {Limit = 1},
					cancellation)
				.ConfigureAwait(false);

			return cursor.TakeOne();
		}
	}
}
