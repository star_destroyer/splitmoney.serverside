﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using SplitMoney.DataAccess.Storage;
using SplitMoney.DataAccess.Utility;
using SplitMoney.ServerModel.Components.Repositories;
using SplitMoney.ServerModel.Configuration;
using SplitMoney.ServerModel.Transactions;

namespace SplitMoney.DataAccess.Repositories
{
    public class TransactionRepository : RepositoryBase<Transaction>, ITransactionRepository
    {
	    public TransactionRepository(IAppConfig config, IClientFactory clientFactory) : base(config, clientFactory)
	    {
	    }

	    public async Task<Transaction> GetTransactionAsync(Guid transactionId)
	    {
		    return await Collection.Find(t => t.Id == transactionId).FirstOrDefaultAsync();
	    }

	    public async Task<Transaction> GetTransactionAsync(long userId, Guid transactionId)
	    {
		    return await Collection.Find(t => t.From == userId && t.Id == transactionId).FirstOrDefaultAsync();
		}

		public async Task CreateOrReplaceTransactionAsync(Transaction transaction)
	    {
			await Collection.ReplaceOneAsync(
			    Builders<Transaction>.Filter.Eq(tran => tran.Id, transaction.Id),
			    transaction,
			    new UpdateOptions { IsUpsert = true }
		    );
	    }

	    public async Task<IEnumerable<Transaction>> GetTransactionsByConversationAsync(long userId, Guid conversationId)
	    {
			//TODO: Проверять, можно ли пользователю смотреть эти транзакции
		    return await Collection.Find(
				    Builders<Transaction>.Filter.Eq(tran => tran.ConversationId, conversationId))
			    .ToListAsync();
	    }
    }
}
