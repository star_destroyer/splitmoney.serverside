﻿using System;
using MongoDB.Bson;
using MongoDB.Driver;
using SplitMoney.ServerModel.Configuration;

namespace SplitMoney.DataAccess.Storage
{
    public class ClientFactory : IClientFactory
    {
        private readonly IAppConfig configuration;

        public ClientFactory(IAppConfig configuration)
        {
            this.configuration = configuration;
        }

        public IMongoClient CreateMongoClient()
        {
	        return new MongoClient(GetClientSettings(configuration.Database.MongoServerAddress));
        }

        private static MongoClientSettings GetClientSettings(string serverAddress)
        {
            return new MongoClientSettings
            {
                ConnectionMode = ConnectionMode.Direct,
                ConnectTimeout = TimeSpan.FromSeconds(30),
                GuidRepresentation = GuidRepresentation.CSharpLegacy,
                Server = MongoServerAddress.Parse(serverAddress)
            };
        }
    }
}