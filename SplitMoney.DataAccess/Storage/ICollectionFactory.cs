﻿using System;
using MongoDB.Driver;

namespace SplitMoney.DataAccess.Storage
{
    public interface ICollectionFactory<TEntity>
    {
        IMongoCollection<TEntity> CreateCollection(Type type);
    }
}