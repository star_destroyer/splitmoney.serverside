﻿using MongoDB.Driver;

namespace SplitMoney.DataAccess.Storage
{
    public interface IClientFactory
    {
        IMongoClient CreateMongoClient();
    }
}