﻿using System;
using MongoDB.Bson;
using MongoDB.Driver;

namespace SplitMoney.DataAccess.Storage
{
    public class CollectionFactory<TEntity> : ICollectionFactory<TEntity>
    {
        private readonly string databaseName;
        private readonly IClientFactory clientFactory;

        public CollectionFactory(string databaseName, IClientFactory clientFactory)
        {
            this.databaseName = databaseName;
            this.clientFactory = clientFactory;
        }

        public IMongoCollection<TEntity> CreateCollection(Type type)
        {
            var storableAttribute = type.GetStorableAttribute();
            var mongoClient = clientFactory.CreateMongoClient();
            var dbName = storableAttribute.DbName ?? databaseName;

            return mongoClient.GetDatabase(dbName, GetMongoDatabaseSettings())
                .GetCollection<TEntity>(storableAttribute.CollectionName, GetMongoCollectionSettings());
        }

        private static MongoDatabaseSettings GetMongoDatabaseSettings()
        {
            return new MongoDatabaseSettings
            {
                GuidRepresentation = GuidRepresentation.CSharpLegacy
            };
        }

        private static MongoCollectionSettings GetMongoCollectionSettings()
        {
            return new MongoCollectionSettings
            {
                AssignIdOnInsert = false,
                GuidRepresentation = GuidRepresentation.CSharpLegacy
            };
        }
    }
}
