﻿using MongoDB.Driver;
using SplitMoney.ServerModel.Configuration;

namespace SplitMoney.DataAccess.Storage
{
    public abstract class RepositoryBase<TEntity>
    {
        protected RepositoryBase(IAppConfig config, IClientFactory clientFactory) 
            : this(new CollectionFactory<TEntity>(config.Database.MongoDatabaseName, clientFactory))
        {
        }

        protected RepositoryBase(ICollectionFactory<TEntity> collectionFactory)
        {
            Collection = collectionFactory.CreateCollection(typeof(TEntity));
        }

        protected IMongoCollection<TEntity> Collection { get; }
    }
}