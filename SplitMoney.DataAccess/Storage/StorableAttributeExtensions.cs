﻿using System;
using System.Collections.Concurrent;
using System.Reflection;
using SplitMoney.ServerModel;

namespace SplitMoney.DataAccess.Storage
{
    public static class StorableAttributeExtensions
    {
        private static readonly ConcurrentDictionary<Type, StorableAttribute> storableAttributeCache 
			= new ConcurrentDictionary<Type, StorableAttribute>();

        public static StorableAttribute GetStorableAttribute(this Type type)
        {
            return storableAttributeCache.GetOrAdd(type, t =>
            {
                var attribute = (StorableAttribute)t.GetTypeInfo().GetCustomAttribute(typeof(StorableAttribute));
                if (attribute == null)
                    throw new InvalidOperationException("Cannot find the StorableAttribute for type " + t.Name);
                return attribute;
            });
        }
    }
}