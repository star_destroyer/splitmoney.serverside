﻿namespace SplitMoney.ServerModel.Users
{
    public class QiwiWallet
    {
	    public const string PhoneNumberKey = "phonenumber";

	    public QiwiWallet(PaymentMethod method)
	    {
		    PhoneNumber = long.Parse(method.PaymentInfo[PhoneNumberKey]);
	    }

		public long PhoneNumber { get; set; }
    }
}
