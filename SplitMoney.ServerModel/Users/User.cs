﻿using System;
using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;

namespace SplitMoney.ServerModel.Users
{
	[Storable("users")]
    public class User
    {
        public long Id { get; set; }
		[BsonIgnoreIfNull]
	    public IEnumerable<PaymentMethod> PaymentMethods;
		[BsonIgnoreIfNull]
		public Dictionary<Guid, string> ConversationNames { get; set; }
		[BsonIgnoreIfNull]
		public Dictionary<Guid, DateTime> ConversationLastCheckDates { get; set; }
	}
}
