﻿using System.Collections.Generic;
using SplitMoney.ServerModel.Transactions;

namespace SplitMoney.ServerModel.Users
{
    public class PaymentMethod : Entity
    {
		public PaymentType Type { get; set; }
		public Dictionary<string, string> PaymentInfo { get; set; }
    }
}
