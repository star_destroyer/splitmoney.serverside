﻿using System;

namespace SplitMoney.ServerModel
{
    public abstract class Entity
    {
	    public Entity() : this(Guid.NewGuid())
	    {
	    }

	    public Entity(Guid id)
	    {
		    Id = id;
	    }

		public Guid Id { get; set; }
    }
}
