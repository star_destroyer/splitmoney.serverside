﻿namespace SplitMoney.ServerModel.Transactions
{
    public enum Confirmation
    {
		Confirm,
		Reject,
		ConfirmationCode
    }
}
