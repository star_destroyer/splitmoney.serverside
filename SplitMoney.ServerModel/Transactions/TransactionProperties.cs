﻿namespace SplitMoney.ServerModel.Transactions
{
	public static class TransactionProperties
	{
		public const string QiwiAuthCode = "qiwi_auth_code";
		public const string QiwiTransactions = "qiwi_transactions";
	}
}