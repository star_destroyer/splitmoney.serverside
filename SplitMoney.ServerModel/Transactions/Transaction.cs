﻿using System;
using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;

namespace SplitMoney.ServerModel.Transactions
{
	[Storable("transactions")]
    public class Transaction : Entity
    {
	    public long From { get; set; }
		public string Comment { get; set; }
		public DateTime Created { get; set; }
		[BsonIgnoreIfNull]
		public DateTime? Finished { get; set; }
	    public Guid ConversationId { get; set; }
	    public PaymentType Type { get; set; }
	    public decimal Amount { get; set; }
	    public Currency Currency { get; set; }
	    public TransactionStatus Status { get; set; }
		[BsonIgnoreIfNull]
	    public Dictionary<string, object> Properties { get; set; }
    }
}
