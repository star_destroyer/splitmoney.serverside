﻿using System;
using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;

namespace SplitMoney.ServerModel.UserPreferences
{
	[Storable("user_preferences")]
	public class UserPreferences
	{
		//userId
		public long Id { get; set; }
		[BsonIgnoreIfNull]
		public Dictionary<Guid, string> ConversationNames { get; set; }
	}
}