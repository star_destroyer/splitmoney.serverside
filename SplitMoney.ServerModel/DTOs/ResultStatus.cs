﻿namespace SplitMoney.ServerModel
{
    public enum ResultStatus
    {
		Ok, 
		ServerError,
		ArgumentError, 
		NotFound
    }
}
