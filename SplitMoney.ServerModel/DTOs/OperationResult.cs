﻿namespace SplitMoney.ServerModel
{
    public class OperationResult<T>
    {
		public T Content { get; set; }
		public ResultStatus Status { get; set; }
		public string Message { get; set; }
    }
}
