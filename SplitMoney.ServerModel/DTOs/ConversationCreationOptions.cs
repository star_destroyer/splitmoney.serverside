﻿using System;
using System.Collections.Generic;

namespace SplitMoney.ServerModel.DTOs
{
    public class ConversationCreationOptions
    {
		public string Name { get; set; }
		public IEnumerable<long> Participants { get; set; }
		public Guid? ConversationId { get; set; }
    }
}
