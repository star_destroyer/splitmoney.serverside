﻿using System;
using System.Collections.Generic;
using SplitMoney.ServerModel.Transactions;

namespace SplitMoney.ServerModel.DTOs
{
    public class TransactionConfirmationOptions
    {
	    public Guid TransactionId { get; set; }
	    public Confirmation? ConfirmationType { get; set; }
		public Dictionary<string, string> Options { get; set; }
	}
}
