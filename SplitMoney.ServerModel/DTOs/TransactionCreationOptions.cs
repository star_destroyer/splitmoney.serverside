﻿using System;
using SplitMoney.ServerModel.Transactions;

namespace SplitMoney.ServerModel.DTOs
{
	public class TransactionCreationOptions
	{
		public string Message { get; set; }
		public Guid RecipientConversation { get; set; }
		public decimal Amount { get; set; }
		public Currency Currency { get; set; }
		public PaymentType PaymentType { get; set; }
		public Guid? PaymentMethod { get; set; }
	}
}
