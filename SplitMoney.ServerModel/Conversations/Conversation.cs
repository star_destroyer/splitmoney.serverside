﻿using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;

namespace SplitMoney.ServerModel.Conversations
{
	[Storable("conversations")]
    public class Conversation : Entity
    {
		[BsonIgnoreIfNull]
	    public IEnumerable<long> Participants { get; set; }
	    public ConversationType Type { get; set; }
    }
}
