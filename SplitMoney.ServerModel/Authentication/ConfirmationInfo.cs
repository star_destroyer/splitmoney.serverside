namespace SplitMoney.ServerModel.Authentication
{
    public class ConfirmationInfo
    {
        public string Phone { get; set; }

        public string Code { get; set; }

        public string SmsCode { get; set; }
    }
}