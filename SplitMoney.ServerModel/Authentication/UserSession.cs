﻿namespace SplitMoney.ServerModel.Authentication
{
	[Storable("sessions")]
    public class UserSession : Entity
    {
        public long Phone { get; set; }

        public string Code { get; set; }

        public string SmsCode { get; set; }

        public string Token { get; set; }

        public string RefreshToken { get; set; }

        public long Expires { get; set; }
    }
}