﻿using SplitMoney.ServerModel.Users;

namespace SplitMoney.ServerModel.Authentication
{
    public class TokenValidationInfo
    {
        public TokenValidationResult Result { get; set; }

        public User User { get; set; }
    }
}