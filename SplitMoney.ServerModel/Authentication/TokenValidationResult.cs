﻿namespace SplitMoney.ServerModel.Authentication
{
    public enum TokenValidationResult
    {
        Ok = 0,
        Expired = 1,
        NotFound = 2
    }
}