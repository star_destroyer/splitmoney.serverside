﻿namespace SplitMoney.ServerModel.Authentication
{
    public class AccessTokenInfo
    {
        public bool IsSuccess { get; set; }

        public string Message { get; set; }

        public string Token { get; set; }

        public string RefreshToken { get; set; }
    }
}