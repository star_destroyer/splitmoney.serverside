﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SplitMoney.ServerModel.Transactions;

namespace SplitMoney.ServerModel.Components.Repositories
{
	public interface ITransactionRepository
	{
		Task CreateOrReplaceTransactionAsync(Transaction transaction);
	    Task<Transaction> GetTransactionAsync(Guid transactionId);
	    Task<Transaction> GetTransactionAsync(long userId, Guid transactionId);
	    Task<IEnumerable<Transaction>> GetTransactionsByConversationAsync(long userId, Guid conversationId);
    }
}
