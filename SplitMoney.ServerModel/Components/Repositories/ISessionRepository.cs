﻿using System.Threading;
using System.Threading.Tasks;
using SplitMoney.ServerModel.Authentication;

namespace SplitMoney.ServerModel.Components.Repositories
{
    public interface ISessionRepository
    {
        Task WriteAsync(UserSession session, CancellationToken cancellation);

        Task<UserSession> GetByPhoneAndCodeAsync(long phone, string code, CancellationToken cancellation);

        Task<UserSession> GetByPhoneAndTokenAsync(long phone, string token, CancellationToken cancellation);
    }
}