﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SplitMoney.ServerModel.Conversations;

namespace SplitMoney.ServerModel.Components.Repositories
{
	public interface IConversationsRepository
	{
		Task<Conversation> GetConversationAsync(Guid id);
		Task<IEnumerable<Conversation>> GetConversationsAsync(long ownerId);
		Task CreateOrUpdateConversationAsync(Conversation conversation);
	}
}
