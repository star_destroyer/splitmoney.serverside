﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SplitMoney.ServerModel.Users;

namespace SplitMoney.ServerModel.Components.Repositories
{
	public interface IUsersRepository
	{
		Task CreateUserAsync(long id);
		Task<User> GetUserAsync(long id);

		Task AddPaymentMethodAsync(long user, PaymentMethod method);
		Task<IEnumerable<PaymentMethod>> GetPaymentMethodsAsync(long user);

		Task AddOrUpdateConversationNameAsync(long owner, Guid id, string name);

		Task SetLastConversationCheckTime(long user, Guid conversationId, DateTime time);
	}
}
