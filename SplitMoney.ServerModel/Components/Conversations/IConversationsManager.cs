﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SplitMoney.ServerModel.Conversations;

namespace SplitMoney.ServerModel.Components.Conversations
{
	public interface IConversationsManager
	{
		Task<OperationResult<Conversation>> CreateOrUpdateConversationAsync(long owner, IEnumerable<long> participants, string name, Guid? id);
	}
}
