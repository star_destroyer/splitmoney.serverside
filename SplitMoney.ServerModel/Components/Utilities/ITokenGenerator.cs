﻿namespace SplitMoney.ServerModel.Components.Utilities
{
    public interface ITokenGenerator
    {
        string NewToken();
    }
}
