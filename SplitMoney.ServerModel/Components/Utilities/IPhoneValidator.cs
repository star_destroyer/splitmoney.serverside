﻿namespace SplitMoney.ServerModel.Components.Utilities
{
    public interface IPhoneValidator
    {
        bool Validate(string phone, out string formatedPhone);
    }
}