﻿using System;

namespace SplitMoney.ServerModel.Components.Utilities
{
    public interface IDateTimeService
    {
        DateTime Now { get; }

        DateTime UtcNow { get; }
    }
}