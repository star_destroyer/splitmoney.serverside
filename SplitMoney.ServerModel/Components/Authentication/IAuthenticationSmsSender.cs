using System.Threading.Tasks;

namespace SplitMoney.ServerModel.Components.Authentication
{
    public interface IAuthenticationSmsSender
    {
        Task<string> SendAuthenticationCodeAsync(long phone);
    }
}