using System.Threading;
using System.Threading.Tasks;
using SplitMoney.ServerModel.Authentication;

namespace SplitMoney.ServerModel.Components.Authentication
{
    public interface IAuthenticationManager
    {
        Task<CodeInfo> CreateCodeAsync(long phone, CancellationToken cancellation);

        Task<AccessTokenInfo> GetTokenAsync(long phone, string code, string smsCode, CancellationToken cancellation);

        Task<TokenValidationInfo> ValidateAsync(long phone, string token, CancellationToken cancellation);
    }
}