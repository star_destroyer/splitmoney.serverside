﻿namespace SplitMoney.ServerModel.Components.Authentication
{
    public interface IAuthenticationTokenSerializer
    {
        (long phone, string token) Deserialize(string authArgument);
    }
}