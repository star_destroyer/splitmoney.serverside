﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SplitMoney.ServerModel.Transactions;

namespace SplitMoney.ServerModel.Components.Transactions
{
    public interface ITransactionManager
    {
	    Task<Transaction> CreateTransactionAsync(
		    long userId,
		    Guid recipientConversation,
		    Guid paymentMethodId,
		    string comment,
		    decimal amount,
		    Currency currency);

	    Task<(Transaction transaction, string message)> ConfirmTransactionAsync(
			Guid transactionId,
			Confirmation confirmationType,
			Dictionary<string, string> confirmationOptions
	    );
    }
}
