﻿using SplitMoney.ServerModel.Transactions;

namespace SplitMoney.ServerModel.Components.Transactions
{
    public interface ITransactionManagerFactory
    {
	    ITransactionManager GetTransactionManager(PaymentType paymentType);
    }
}
