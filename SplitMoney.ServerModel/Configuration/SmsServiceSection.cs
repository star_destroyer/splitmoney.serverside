﻿namespace SplitMoney.ServerModel.Configuration
{
    public class SmsServiceSection
    {
		public bool EnableMessages { get; set; }
		public string User { get; set; }
		public string ApiKey { get; set; }
    }
}
