namespace SplitMoney.ServerModel.Configuration
{
	public class DatabaseSection
	{
		public string MongoServerAddress { get; set; }
		public string MongoDatabaseName { get; set; }
	}
}