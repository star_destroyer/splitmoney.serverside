﻿namespace SplitMoney.ServerModel.Configuration
{
	public interface IAppConfig
	{
		DatabaseSection Database { get; set; }
		LoggingSection Logging { get; set; }
		SmsServiceSection SmsService { get; set; }
	}
}