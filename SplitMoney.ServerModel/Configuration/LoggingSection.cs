namespace SplitMoney.ServerModel.Configuration
{
	public class LoggingSection
	{
		public string LogFileFormat { get; set; }
		public bool IncludeScopes { get; set; }
		public LogLevelSection LogLevel { get; set; }
	}
}