namespace SplitMoney.ServerModel.Configuration
{
	public class AppConfig : IAppConfig
	{
		public LoggingSection Logging { get; set; }
		public DatabaseSection Database { get; set; }
		public SmsServiceSection SmsService { get; set; }
	}
}