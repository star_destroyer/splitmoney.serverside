namespace SplitMoney.ServerModel.Configuration
{
	public class LogLevelSection
	{
		public string Default { get; set; }
		public string Serilog { get; set; }
	}
}