﻿using System;

namespace SplitMoney.ServerModel
{
    [AttributeUsage(AttributeTargets.Class)]
    public class StorableAttribute : Attribute
    {
        public string DbName { get; set; }
        public string CollectionName { get; }

        public StorableAttribute(string collectionName)
        {
            CollectionName = collectionName;
        }
    }
}