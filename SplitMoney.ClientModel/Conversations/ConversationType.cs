﻿namespace SplitMoney.ClientModel.Conversations
{
    public enum ConversationType
    {
        Dialog,
        Group
    }
}