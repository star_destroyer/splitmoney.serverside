﻿using System;
using System.Collections.Generic;
using SplitMoney.ClientModel.Users;

namespace SplitMoney.ClientModel.Conversations
{
    public class Conversation
    {
        public Guid Id { get; set; }
		public string Name { get; set; }
		public DateTime LastEvent { get; set; }
	    public decimal Balance { get; set; }
		public int Notifications { get; set; }
	    public ConversationType Type { get; set; }
	    public IEnumerable<User> Participants { get; set; }
    }
}
