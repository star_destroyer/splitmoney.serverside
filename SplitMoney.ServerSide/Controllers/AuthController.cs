﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SplitMoney.Business.Utilities;
using SplitMoney.ServerModel.Authentication;
using SplitMoney.ServerModel.Components.Authentication;

namespace SplitMoney.ServerSide.Controllers
{
	[AllowAnonymous]
	[Route("auth")]
	public class AuthController : Controller
	{
		private readonly IAuthenticationManager authenticationManager;

		public AuthController(IAuthenticationManager authenticationManager)
		{
			this.authenticationManager = authenticationManager;
		}

		[HttpPost("authenticate")]
		public async Task<ActionResult> BeginAuthentication([FromForm] AuthorizationInfo authorizationInfo)
		{
			if (authorizationInfo.Phone == null)
			{
				return BadRequest("Bad parameters");
			}

			var codePermission = await authenticationManager.CreateCodeAsync(
					authorizationInfo.Phone.ToInt64PhoneNumber(),
					ControllerContext.HttpContext.RequestAborted)
				.ConfigureAwait(false);

			return new OkObjectResult(codePermission);
		}

		[HttpPost("obtainToken")]
		public async Task<ActionResult> EndAuthentication([FromForm] ConfirmationInfo confirmationInfo)
		{
			if (confirmationInfo.Phone == null || confirmationInfo.Code == null || confirmationInfo.SmsCode == null)
			{
				return BadRequest("Bad parameters");
			}

			var tokenPermission = await authenticationManager.GetTokenAsync(
					confirmationInfo.Phone.ToInt64PhoneNumber(),
					confirmationInfo.Code,
					confirmationInfo.SmsCode,
					ControllerContext.HttpContext.RequestAborted)
				.ConfigureAwait(false);

			if (tokenPermission.IsSuccess)
			{
				return new OkObjectResult(tokenPermission);
			}
			else
			{
				return new BadRequestObjectResult(tokenPermission);
			}
		}
	}
}