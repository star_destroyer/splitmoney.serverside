﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SplitMoney.QiwiClient;
using SplitMoney.ServerModel.Components.Repositories;
using SplitMoney.ServerModel.Components.Transactions;
using SplitMoney.ServerModel.DTOs;
using SplitMoney.ServerModel.Transactions;
using SplitMoney.ServerSide.Utilities;
using ClientTransaction = SplitMoney.ClientModel.Transactions.Transaction;

namespace SplitMoney.ServerSide.Controllers
{
	[Route("transactions")]
    public class TransactionsController : Controller
	{
		private readonly ITransactionManagerFactory transactionManagerFactory;
		private readonly ITransactionRepository transactionRepository;
		private readonly ILogger logger;
		private readonly IMapper mapper;

		public TransactionsController(ITransactionManagerFactory transactionManagerFactory, ITransactionRepository transactionRepository, IMapper mapper, ILoggerFactory loggerFactory)
		{
			this.transactionManagerFactory = transactionManagerFactory;
			this.transactionRepository = transactionRepository;
			this.mapper = mapper;
			this.logger = loggerFactory.CreateLogger(typeof(TransactionsController));
		}

		[HttpGet("get")]
		public async Task<ActionResult> GetTransactionAsync([FromBody] Guid transactionId)
		{
			var transaction = await transactionRepository.GetTransactionAsync(HttpContext.GetUserId(), transactionId);

			var clientTransaction = mapper.Map<ClientTransaction>(transaction);
			clientTransaction.Message = transaction.Comment;
			return Ok(new {Transaction = clientTransaction});
		}

		[HttpPost("create")]
		public async Task<ActionResult> PostTransactionAsync([FromBody] TransactionCreationOptions transaction)
		{
			var transactionManager = transactionManagerFactory.GetTransactionManager(transaction.PaymentType); //TODO: хардкод, нужно резолвить из PaymentMethodId.
			var resultingTransaction = await transactionManager.CreateTransactionAsync(
					HttpContext.GetUserId(),
					transaction.RecipientConversation,
					transaction.PaymentMethod ?? Guid.Empty,
					transaction.Message,
					transaction.Amount,
					transaction.Currency
				);

			var clientTransaction = mapper.Map<ClientTransaction>(resultingTransaction);
			return Ok(new {Transaction = clientTransaction});
		}

		[HttpPost("confirm")]
		public async Task<ActionResult> ConfirmTransactionAsync([FromBody] TransactionConfirmationOptions confirmationOptions)
		{
			var transaction = await transactionRepository.GetTransactionAsync(confirmationOptions.TransactionId);
			var transactionManager = transactionManagerFactory.GetTransactionManager(transaction.Type); //TODO: хардкод, нужно резолвить из TransactionId на уровне TransactionManager.
			
			try
			{
				var confirmationResult = await transactionManager.ConfirmTransactionAsync(
					confirmationOptions.TransactionId,
					confirmationOptions.ConfirmationType ?? Confirmation.ConfirmationCode,
					confirmationOptions.Options);

				var clientTransaction = mapper.Map<ClientTransaction>(confirmationResult.transaction);
				clientTransaction.Message = confirmationResult.transaction.Comment;
				return Ok(new { Transaction = clientTransaction });
			}
			catch (QiwiException e)
			{
				return BadRequest(new {Message = e.Message});
			}
		}

		[HttpPost("list")]
		public async Task<ActionResult> GetTransactionsAsync([FromBody] Guid conversationId)
		{
			var userId = HttpContext.GetUserId();
			var transactions = await transactionRepository.GetTransactionsByConversationAsync(userId, conversationId);

			var clientTransactions = transactions
				.Select(t =>
				{
					var clientTransaction = mapper.Map<ClientTransaction>(t);
					clientTransaction.Message = t.Comment;
					return clientTransaction;
				});
			return Ok(new {Transactions = clientTransactions});
		}
	}
}
