﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SplitMoney.ServerModel.Components.Repositories;
using SplitMoney.ServerModel.Users;
using SplitMoney.ServerSide.Utilities;

namespace SplitMoney.ServerSide.Controllers
{
	[Route("user")]
    public class UsersController : Controller
	{
		private readonly IUsersRepository usersRepository;

		public UsersController(IUsersRepository usersRepository)
		{
			this.usersRepository = usersRepository;
		}

		[HttpGet("get")]
	    public async Task<ActionResult> GetUserAsync()
		{
			var user = await usersRepository.GetUserAsync(HttpContext.GetUserId());

			return Ok(user);
		}

		[HttpPost("paymentMethod")]
		public async Task<ActionResult> AddPaymentMethod([FromBody] PaymentMethod paymentMethod)
		{
			var userId = HttpContext.GetUserId();
			await usersRepository.AddPaymentMethodAsync(userId, paymentMethod);

			return Ok();
		}
    }
}
