﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SplitMoney.ClientModel.Users;
using SplitMoney.ServerModel;
using SplitMoney.ServerModel.Components.Conversations;
using SplitMoney.ServerModel.Components.Repositories;
using SplitMoney.ServerModel.Conversations;
using SplitMoney.ServerModel.DTOs;
using SplitMoney.ServerSide.Utilities;

using ServerTransaction = SplitMoney.ServerModel.Transactions.Transaction;

namespace SplitMoney.ServerSide.Controllers
{
	[Route("conversations")]
    public class ConversationsController : Controller
	{
		private readonly IConversationsRepository conversationsRepository;
		private readonly IConversationsManager conversationsManager;
		private readonly IUsersRepository usersRepository;
		private readonly ITransactionRepository transactionRepository;
		private readonly IMapper mapper;
		private readonly ILogger logger;

		public ConversationsController(
			IConversationsRepository conversationsRepository, 
			ITransactionRepository transactionRepository, 
			IConversationsManager conversationsManager, 
			IMapper mapper, 
			IUsersRepository usersRepository, ILoggerFactory loggerFactory)
		{
			this.conversationsRepository = conversationsRepository;
			this.conversationsManager = conversationsManager;
			this.mapper = mapper;
			this.usersRepository = usersRepository;
			this.transactionRepository = transactionRepository;
			this.logger = loggerFactory.CreateLogger<ConversationsController>();
		}

		[HttpGet("list")]
		public async Task<ActionResult> GetUserConversations()
		{
			var userId = HttpContext.GetUserId();
			var conversationsList = await conversationsRepository.GetConversationsAsync(userId);
			var user = await usersRepository.GetUserAsync(userId);
			var names = user.ConversationNames;

			var conversations = conversationsList.Select(
				conv =>
				{
					var transactions = transactionRepository.GetTransactionsByConversationAsync(userId, conv.Id).Result.ToList();
					return new ClientModel.Conversations.Conversation
					{
						Id = conv.Id,
						Name = GetConversationName(conv, names),
						LastEvent = GetLastEventDate(transactions),
						Balance = GetConversationBalance(transactions, userId).Result,
						Participants = conv.Participants.Select(p => new User {Id = p}),
						Notifications = GetNotificationCount(transactions, conv.Id, user),
						Type = mapper.Map<ClientModel.Conversations.ConversationType>(conv.Type)
					};
				});
			return Ok(new {Conversations = conversations});
		}

		private static string GetConversationName(Conversation conv, Dictionary<Guid, string> names)
		{
			return names?.ContainsKey(conv.Id) ?? false
				? names[conv.Id]
				: string.Join(", ", conv.Participants);
		}

		[HttpPost("createOrUpdate")]
		public async Task<ActionResult> CreateOrUpdateConversation([FromBody] ConversationCreationOptions options)
		{
			if (options.Participants == null)
			{
				return BadRequest(new {message = $"Отсутствует параметр {nameof(options.Participants)}"});
			}

			var userId = HttpContext.GetUserId();
			var result = await conversationsManager.CreateOrUpdateConversationAsync(
				userId,
				options.Participants,
				options.Name,
				options.ConversationId);

			if (result.Status == ResultStatus.Ok)
			{
				var names = (await usersRepository.GetUserAsync(userId)).ConversationNames;
				var conv = result.Content;
				var transactions = (await transactionRepository.GetTransactionsByConversationAsync(userId, conv.Id).ConfigureAwait(false))
					.ToList();
				var user = await usersRepository.GetUserAsync(userId);
				var clientConversation = new ClientModel.Conversations.Conversation
				{
					Id = conv.Id,
					Name = GetConversationName(conv, names),
					LastEvent = GetLastEventDate(transactions),
					Balance = await GetConversationBalance(transactions, userId),
					Participants = conv.Participants.Select(p => new User { Id = p }),
					Notifications = GetNotificationCount(transactions, conv.Id, user),
					Type = mapper.Map<ClientModel.Conversations.ConversationType>(conv.Type)
				};
				return Ok(clientConversation);
			}
			return new JsonResult(result) {StatusCode = result.Status.ToStatusCode()};
		}

		private int GetNotificationCount(IEnumerable<ServerTransaction> transactions, Guid conversationId, ServerModel.Users.User user)
		{
			var checkDates = user.ConversationLastCheckDates;
			if (checkDates == null)
				return transactions.Count();
			if (!checkDates.TryGetValue(conversationId, out DateTime lastCheckDate))
				return transactions.Count();
			return transactions.Count(t => t.Created > lastCheckDate || t.Finished > lastCheckDate);
		}

		private async Task<decimal> GetConversationBalance(IEnumerable<ServerTransaction> transactions, long userId)
		{
			var confirmedTransactions = transactions
				.Where(t => t.Status == ServerModel.Transactions.TransactionStatus.Confirmed)
				.ToList();
			var usersDebt = confirmedTransactions
				.Where(t => t.From != userId)
				.DefaultIfEmpty()
				.Sum(t => t?.Amount ?? 0);
			var othersDebt = confirmedTransactions
				.Where(t => t.From == userId)
				.DefaultIfEmpty()
				.Sum(t => t?.Amount ?? 0);
			return othersDebt - usersDebt;
		}

		private DateTime GetLastEventDate(IEnumerable<ServerTransaction> transactions)
		{
			return transactions
				.DefaultIfEmpty()
				.Select(t => t?.Created ?? DateTime.Now)
				.Max();
		}
	}
}
