﻿using System;
using System.Linq;
using System.Reflection;
using Autofac;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Events;
using SplitMoney.DataAccess.Repositories;
using SplitMoney.QiwiClient;
using SplitMoney.ServerModel.Components.Repositories;
using SplitMoney.ServerModel.Configuration;
using SplitMoney.ServerSide.Middlewares;
using Swashbuckle.AspNetCore.Swagger;

namespace SplitMoney.ServerSide
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();

	        var loggingConfig = Configuration.Get<AppConfig>().Logging;
	        var logEventLevel = Enum.TryParse<LogEventLevel>(loggingConfig.LogLevel.Serilog, out var level) 
				? level 
				: LogEventLevel.Information;
	        Log.Logger = new LoggerConfiguration()
				.MinimumLevel.Is((LogEventLevel) Enum.Parse(typeof(LogEventLevel), loggingConfig.LogLevel.Serilog))
				.MinimumLevel.Is(logEventLevel)
		        .WriteTo.RollingFile(loggingConfig.LogFileFormat)
		        .CreateLogger();
        }

        public static IConfigurationRoot Configuration { get; private set; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

	        services.AddSwaggerGen(c =>
	        {
		        c.SwaggerDoc("v1", new Info {Title = "SplitMoney API", Version = "v1"});
	        });
        }

	    public void ConfigureContainer(ContainerBuilder builder)
	    {
		    builder.RegisterAssemblyTypes(Assembly.GetEntryAssembly()
				    .GetReferencedAssemblies()
				    .Where(x => x.Name.StartsWith("SplitMoney"))
				    .Select(Assembly.Load)
				    .Concat(new[] {Assembly.GetEntryAssembly()})
				    .ToArray())
			    .AsImplementedInterfaces()
			    .SingleInstance();

		    builder.Register(
				    context => new Mapper(new MapperConfiguration(expression =>
				    {
					    expression.CreateMissingTypeMaps = true;
					    expression.AllowNullCollections = true;
				    })))
			    .As<IMapper>();

			builder.Register(context => Configuration.Get<AppConfig>()).As<IAppConfig>().InstancePerLifetimeScope();
			builder.RegisterType<MongoSessionRepository>().As<ISessionRepository>().InstancePerLifetimeScope();
		    builder.RegisterType<QiwiClientFactory>().As<IQiwiClientFactory>();
	    }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
	        loggerFactory.AddSerilog();

            app
				.UseSmsAuthorization()
				.UseSwagger()
				.UseSwaggerUI(options => options.SwaggerEndpoint("/swagger/v1/swagger.json", "SplitMoney API"))
				.UseMvc();
        }
    }
}
