﻿using Microsoft.AspNetCore.Http;
using SplitMoney.Business.Utilities;

namespace SplitMoney.ServerSide.Utilities
{
    public static class HttpContextExtensions
    {
	    public static long GetUserId(this HttpContext context)
	    {
		    return context.User.Identity.Name.ToInt64PhoneNumber();
	    }
    }
}
