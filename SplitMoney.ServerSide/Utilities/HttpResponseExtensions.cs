﻿using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace SplitMoney.ServerSide.Utilities
{
    public static class HttpResponseExtensions
    {
		public static Task WriteErrorResponse(this HttpResponse response, HttpStatusCode code, string message)
	    {
		    return response.WriteErrorResponse(code, new {message});
	    }

	    public static async Task WriteErrorResponse(this HttpResponse response, HttpStatusCode code, object message = null)
	    {
		    response.StatusCode = (int) code;
		    if (message == null)
			    return;
		    await response
				.WriteAsync(JsonConvert.SerializeObject(message))
				.ConfigureAwait(false);
	    }
    }
}
