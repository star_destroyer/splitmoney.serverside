﻿using System;
using SplitMoney.ServerModel;

namespace SplitMoney.ServerSide.Utilities
{
    public static class Extensions
    {
	    public static int ToStatusCode(this ResultStatus status)
	    {
		    switch (status)
		    {
			    case ResultStatus.Ok:
				    return 200;
			    case ResultStatus.ServerError:
				    return 500;
			    case ResultStatus.ArgumentError:
				    return 400;
				case ResultStatus.NotFound:
					return 404;
			    default:
				    throw new ArgumentOutOfRangeException(nameof(status), status, null);
		    }
	    }
    }
}
