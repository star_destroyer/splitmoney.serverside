﻿using Microsoft.AspNetCore.Builder;

namespace SplitMoney.ServerSide.Middlewares
{
    public static class MiddlewareExtensions
    {
        public static IApplicationBuilder UseSmsAuthorization(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<AuthorizationMiddleware>();
        }
    }
}