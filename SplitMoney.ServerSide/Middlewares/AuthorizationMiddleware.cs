using System;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using SplitMoney.ServerModel.Authentication;
using SplitMoney.ServerModel.Components.Authentication;
using SplitMoney.ServerSide.Utilities;

namespace SplitMoney.ServerSide.Middlewares
{
    public class AuthorizationMiddleware
    {
        private readonly RequestDelegate next;
        private readonly IAuthenticationManager authenticationManager;
        private readonly IAuthenticationTokenSerializer tokenSerializer;
		
        public AuthorizationMiddleware(
            RequestDelegate next,
            IAuthenticationManager authenticationManager,
            IAuthenticationTokenSerializer tokenSerializer)
        {
            this.next = next;
            this.authenticationManager = authenticationManager;
            this.tokenSerializer = tokenSerializer;
        }

        public async Task Invoke(HttpContext context)
        {
            if (context.Request.Path.StartsWithSegments(PathString.FromUriComponent("/auth"))
				|| context.Request.Path.StartsWithSegments(PathString.FromUriComponent("/swagger")))
            {
                await next(context).ConfigureAwait(false);
                return;
            }

            var auth = context.Request.Headers["Authorization"].FirstOrDefault();
            if (auth == null)
            {
                await context.Response
					.WriteErrorResponse(HttpStatusCode.Unauthorized)
					.ConfigureAwait(false);
                return;
            }

            var authParts = auth.Split(' ');
            if (authParts[0] != "Token" || authParts.Length < 2)
            {
                await context.Response
					.WriteErrorResponse(HttpStatusCode.Forbidden, "Auth scheme is not supported")
					.ConfigureAwait(false);
                return;
            }

            var (phone, token) = tokenSerializer.Deserialize(authParts[1]);
            var tokenValidationInfo = await authenticationManager.ValidateAsync(phone, token, context.RequestAborted).ConfigureAwait(false);

            await ProcessValidationResult(tokenValidationInfo, context).ConfigureAwait(false);
        }

	    private async Task ProcessValidationResult(TokenValidationInfo tokenValidationInfo, HttpContext context)
	    {
		    switch (tokenValidationInfo.Result)
		    {
			    case TokenValidationResult.Ok:
				    context.User = new ClaimsPrincipal(new PhoneClaims(tokenValidationInfo.User.Id));
				    await next(context).ConfigureAwait(false);
				    break;
			    case TokenValidationResult.Expired:
				    await context.Response.WriteErrorResponse(HttpStatusCode.Forbidden, "Token expired")
					    .ConfigureAwait(false);
				    break;
			    case TokenValidationResult.NotFound:
				    await context.Response.WriteErrorResponse(HttpStatusCode.NotFound).ConfigureAwait(false);
				    break;
			    default:
				    throw new ArgumentOutOfRangeException();
		    }
	    }

	    private class PhoneIdentity : IIdentity
        {
            public PhoneIdentity(long phone)
            {
                Name = phone.ToString();
            }
            public string AuthenticationType => "Token";
            public bool IsAuthenticated => true;
            public string Name { get; }
        }

        private class PhoneClaims : ClaimsPrincipal
        {
            public PhoneClaims(long phone)
                : base(new PhoneIdentity(phone))
            {
            }
        }
    }
}