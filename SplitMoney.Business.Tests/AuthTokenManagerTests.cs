﻿using System;
using System.Threading;
using FakeItEasy;
using NUnit.Framework;
using SplitMoney.Business.Auth;
using SplitMoney.ServerModel.Authentication;
using SplitMoney.ServerModel.Components.Authentication;
using SplitMoney.ServerModel.Components.Repositories;
using SplitMoney.ServerModel.Components.Utilities;

namespace SplitMoney.Business.Tests
{
    [TestFixture]
    public class AuthTokenManagerTests
    {
        private const long Phone = 71234567890;
        private const string Code = "abCDERFGH++==";
        private const string SmsCode = "0000";

        private IAuthenticationManager authenticationManager;
        private ISessionRepository sessionRepository;
        private IDateTimeService dateTimeService;

        [SetUp]
        public void Setup()
        {
            sessionRepository = A.Fake<ISessionRepository>();
            dateTimeService = A.Fake<IDateTimeService>();
            A.CallTo(() => dateTimeService.UtcNow).Returns(DateTime.UtcNow);
            authenticationManager = new AuthenticationManager(sessionRepository, A.Fake<IUsersRepository>(), dateTimeService, A.Fake<IAuthenticationSmsSender>(), A.Fake<ITokenGenerator>());
        }

        [Test]
        public void GetToken_NoSession_NoToken()
        {
            A.CallTo(() => sessionRepository.GetByPhoneAndCodeAsync(A<long>.Ignored, A<string>.Ignored, A<CancellationToken>.Ignored)).Returns((UserSession)null);

            var accessTokenInfo = authenticationManager.GetTokenAsync(Phone, Code, SmsCode, CancellationToken.None).Result;

            Assert.False(accessTokenInfo.IsSuccess);
        }

        [Test]
        public void GetToken_CodeNotMatch_NoToken()
        {
            A.CallTo(() => sessionRepository.GetByPhoneAndCodeAsync(A<long>._, A<string>.Ignored, A<CancellationToken>._)).Returns(CreateSession(code: ""));

            var accessTokenInfo = authenticationManager.GetTokenAsync(Phone, Code, SmsCode, CancellationToken.None).Result;

            Assert.False(accessTokenInfo.IsSuccess);
        }

        [Test]
        public void GetToken_SmsCodeNotMatch_NoToken()
        {
            A.CallTo(() => sessionRepository.GetByPhoneAndCodeAsync(A<long>._, A<string>.Ignored, A<CancellationToken>._)).Returns(CreateSession(sms: ""));

            var accessTokenInfo = authenticationManager.GetTokenAsync(Phone, Code, SmsCode, CancellationToken.None).Result;

            Assert.False(accessTokenInfo.IsSuccess);
        }

        [Test]
        public void GetToken_AllCodesValid_ReturnToken()
        {
            A.CallTo(() => sessionRepository.GetByPhoneAndCodeAsync(A<long>._, A<string>.Ignored, A<CancellationToken>._)).Returns(CreateSession());

            var accessTokenInfo = authenticationManager.GetTokenAsync(Phone, Code, SmsCode, CancellationToken.None).GetAwaiter().GetResult();

            Assert.That(accessTokenInfo.IsSuccess);
        }

        private static UserSession CreateSession(long phone = Phone, string code = Code, string sms = SmsCode)
        {
            return new UserSession
            {
                Phone = phone,
                Code = code,
                SmsCode = sms
            };
        }
    }
}
