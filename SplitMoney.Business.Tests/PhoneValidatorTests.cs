﻿using NUnit.Framework;
using SplitMoney.Business.Utilities;

namespace SplitMoney.Business.Tests
{
    [TestFixture]
    public class PhoneValidatorTests
    {
        private readonly PhoneValidator phoneValidator;
        private string formatedPhone;

        public PhoneValidatorTests()
        {
            phoneValidator = new PhoneValidator();
        }

        [TestCase(null, false, null, TestName = "Validate_NullPhone_ReturnFalse")]
        [TestCase("", false, null, TestName = "Validate_EmptyPhone_ReturnFalse")]
        [TestCase("1234", false, null, TestName = "Validate_ShortPhone_ReturnFalse")]
        [TestCase("7123456789g", false, null, TestName = "Validate_PhoneWithLetter_ReturnFalse")]
        [TestCase("12345678907", false, null, TestName = "Validate_NotStartFromSeven_ReturnFalse")]
        [TestCase("+82345678907", false, null, TestName = "Validate_StartFromEightWithPlus_ReturnFalse")]
        [TestCase("71234567890", true, "71234567890", TestName = "Validate_StartFromSeven_ReturnTrue")]
        [TestCase("+71234567890", true, "71234567890", TestName = "Validate_StartFromSevenAndPlus_ReturnTrue")]
        [TestCase("81234567890", true, "71234567890", TestName = "Validate_StartFromEight_ReturnTrue")]
        public void Validate(string phone, bool expectedResult, string expectedFormatedPhone)
        {
            var acutalResult = phoneValidator.Validate(phone, out formatedPhone);

            Assert.That(acutalResult, Is.EqualTo(expectedResult));
            Assert.That(formatedPhone, Is.EqualTo(expectedFormatedPhone));
        }
    }
}