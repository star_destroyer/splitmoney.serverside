﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace SplitMoney.QiwiClient
{
    public class QiwiClientFactory : IQiwiClientFactory
    {
        private static readonly Uri GetAccesTokenUri = new Uri(@"https://w.qiwi.com/oauth/access_token");
        private static readonly Uri GetCodeUri = new Uri(@"https://w.qiwi.com/oauth/authorize");
        private readonly IQiwiSender qiwiSender;

        public QiwiClientFactory(IQiwiSender qiwiSender)
        {
            this.qiwiSender = qiwiSender;
        }

        public async Task<string> BeginAuthorizationAsync(string sender, CancellationToken cancellation)
        {
            var sendingResult = await qiwiSender.SendAsync(GetCodeHttpMessage(sender), cancellation)
                .ConfigureAwait(false);
            if (!sendingResult.IsSuccess)
                throw new QiwiException(sendingResult.Code, sendingResult.Message);
            return JObject.Parse(sendingResult.Message)["code"].ToString();
        }

        public async Task<IQiwiClient> EndAuthorizationAsync(string code, string smsCode, string sender,
            CancellationToken cancellation)
        {
            var sendingResult = await qiwiSender.SendAsync(GetAccessTokenHttpMessage(code, smsCode), cancellation)
                .ConfigureAwait(false);
            if (!sendingResult.IsSuccess)
                throw new QiwiException(sendingResult.Code, sendingResult.Message);
            return Create(sender, JObject.Parse(sendingResult.Message)["access_token"].ToString());
        }

        public IQiwiClient Create(string sender, string accessToken)
        {
            return new QiwiClient(sender, accessToken, qiwiSender);
        }

        private static HttpRequestMessage GetCodeHttpMessage(string sender)
        {
            var message = new HttpRequestMessage(HttpMethod.Post, GetCodeUri);
            message.Headers.Accept.ParseAdd("*/*");
            message.Headers.AcceptEncoding.ParseAdd("gzip");
            message.Headers.AcceptEncoding.ParseAdd("deflate");
            message.Headers.AcceptEncoding.ParseAdd("compress");
            message.Headers.UserAgent.ParseAdd("HTTPie/0.3.0");
            message.Content = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("client_id", "qw-fintech"),
                new KeyValuePair<string, string>("client_secret", "Xghj!bkjv64"),
                new KeyValuePair<string, string>("client-software", "qw-fintech-0.0.1"),
                new KeyValuePair<string, string>("response_type", "code"),
                new KeyValuePair<string, string>("username", sender)
            });
            return message;
        }

        private static HttpRequestMessage GetAccessTokenHttpMessage(string code, string smsCode)
        {
            var message = new HttpRequestMessage(HttpMethod.Post, GetAccesTokenUri);
            message.Headers.Accept.ParseAdd("*/*");
            message.Headers.AcceptEncoding.ParseAdd("gzip");
            message.Headers.AcceptEncoding.ParseAdd("deflate");
            message.Headers.AcceptEncoding.ParseAdd("compress");
            message.Headers.UserAgent.ParseAdd("HTTPie/0.3.0");
            message.Content = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("client_id", "qw-fintech"),
                new KeyValuePair<string, string>("client_secret", "Xghj!bkjv64"),
                new KeyValuePair<string, string>("client-software", "qw-fintech-0.0.1"),
                new KeyValuePair<string, string>("grant_type", "urn:qiwi:oauth:grant-type:vcode"),
                new KeyValuePair<string, string>("code", code),
                new KeyValuePair<string, string>("vcode", smsCode)
            });
            return message;
        }
    }
}