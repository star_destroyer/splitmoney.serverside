﻿using System;
using System.Net;

namespace SplitMoney.QiwiClient
{
    public class QiwiException : Exception
    {
        public QiwiException(HttpStatusCode code, string message) : base(message)
        {
            Code = code;
        }

        public HttpStatusCode Code { get; }
    }
}