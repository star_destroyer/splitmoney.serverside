﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace SplitMoney.QiwiClient
{
    public class QiwiSender : IQiwiSender
    {
        private HttpClient client;
        private HttpClient Client => client ?? (client = new HttpClient());

        public async Task<SendingResult> SendAsync(HttpRequestMessage message, CancellationToken cancellation)
        {
            var response = await Client.SendAsync(message, cancellation).ConfigureAwait(false);
            var responseMessage = await response.Content.ReadAsStringAsync();
            return new SendingResult(response.IsSuccessStatusCode, response.StatusCode, responseMessage);
        }
    }
}