﻿namespace SplitMoney.QiwiClient
{
    public class QiwiTransaction
    {
        public string Id { get; set; }
        public decimal Amount { get; set; }
        public string Recipient { get; set; }
        public string Sender { get; set; }
        public bool IsSuccess { get; set; }
        public TransactionMessage Message { get; set; }
    }
}
