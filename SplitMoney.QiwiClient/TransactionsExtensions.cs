﻿using System.Collections.Generic;
using System.Linq;

namespace SplitMoney.QiwiClient
{
    public static class TransactionsExtensions
    {
	    public static string GetMessage(this IEnumerable<QiwiTransaction> transactions)
	    {
		    return string.Join(", ", transactions.Select(qt => $"Получатель: {qt.Recipient}. Сообщение: {qt.Message}"));
	    }
    }
}
