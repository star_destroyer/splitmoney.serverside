﻿using Newtonsoft.Json;

namespace SplitMoney.QiwiClient
{
    public class TransactionMessage
    {
		[JsonProperty(PropertyName = "code")]
		public string Code { get; set; }

		[JsonProperty(PropertyName = "message")]
		public string Message { get; set; }
    }
}
